import TUIO.*;
TuioProcessing tuioClient;
float cur_size = 20;
float obj_size = 60;
float table_size = 320;

Button[] Modules = new Button[32];
int button_count = 18;
PFont font;

void setup(){
  frameRate(60);
  tuioClient  = new TuioProcessing(this);
  font = loadFont("BitstreamVeraSans-Roman-10.vlw");
  size(400,343);
  Modules[0] = new Button(35,35,46,16,#7ce26f,"Tog 1");
  Modules[1] = new Button(93,35,46,16,#7ce26f,"Tog 1");
  Modules[2] = new Button(149,35,46,16,#7ce26f,"Tog 3");
  Modules[3] = new Button(207,35,46,16,#7ce26f,"Tog 4");
  Modules[4] = new Button(264,35,46,16,#7ce26f,"Tog 5");
  Modules[5] = new Button(320,35,46,16,#7ce26f,"Tog 6");
  
  Modules[6] = new Button(35,65,46,46,#c90000,"Cont 1");
  Modules[7] = new Button(93,65,46,46,#c90000,"Cont 2");
  Modules[8] = new Button(149,65,46,46,#c90000,"Cont 3");
  Modules[9] = new Button(207,65,46,46,#c90000,"Cont 4");
  Modules[10] = new Button(264,65,46,46,#c90000,"Cont 5");
  Modules[11] = new Button(320,65,46,46,#c90000,"Cont 6");
  
  Modules[12] = new Button(35,123,46,136,#5085e7,"Slid 1");
  Modules[13] = new Button(93,123,46,136,#5085e7,"Slid 2");
  Modules[14] = new Button(149,123,46,136,#5085e7,"Slid 3");
  Modules[15] = new Button(207,123,46,136,#5085e7,"Slid 4");
  Modules[16] = new Button(264,123,46,136,#5085e7,"Slid 5");
  Modules[17] = new Button(320,123,46,136,#5085e7,"Slid 6");
}

void draw(){
  background(0);
  for(int i=0; i< button_count; i++){
    Modules[i].draw();
  }
  
   Vector tuioCursorList = tuioClient.getTuioCursors();
   for (int i=0;i<tuioCursorList.size();i++) {
      TuioCursor tcur = (TuioCursor)tuioCursorList.elementAt(i);
      Vector pointList = tcur.getPath();
      
      if (pointList.size()>0) {
        stroke(0,0,255);
        TuioPoint start_point = (TuioPoint)pointList.firstElement();;
        for (int j=0;j<pointList.size();j++) {
           TuioPoint end_point = (TuioPoint)pointList.elementAt(j);
           line(start_point.getScreenX(width),start_point.getScreenY(height),end_point.getScreenX(width),end_point.getScreenY(height));
           start_point = end_point;
        }
        
        noStroke();
        fill(192,192,192);
        smooth();
        ellipse( tcur.getScreenX(width), tcur.getScreenY(height),cur_size,cur_size);
        fill(0);
        text(""+ tcur.getCursorID(),  tcur.getScreenX(width)-9,  tcur.getScreenY(height)+4);
      }
   }
}

void mouseReleased(){
}
