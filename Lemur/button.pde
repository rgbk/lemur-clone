class Button{
  boolean toggle, on;
  int x, y, w, h;
  color c;
  String name;

  Button(int x, int y, int w, int h, color c){
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.c = c;
  }

  Button(int x, int y, int w, int h, color c, String name){
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.c = c;
    this.name = name;
  }

  void draw(){
    stroke(c);
    noFill();
    rect(x,y,w,h);
    if(name != null){
      fill(255);
      textFont(font);
      text(name,x,y-2);
    }
    if(on){
      fill(c);
      noStroke();
      int radius = min(w,h) / 2;
      smooth();
      ellipse(x+w/2, y+h/2, radius, radius);
      noSmooth();
    }
  }
  
  void collision(int bx, int by, int ex, int ey){
    if(/*begin*/ bx > x && bx < x+w && by > y && by < y+h
      /*end*/ && ex > x && ex < x+w && ey > y && ey < y+h){
      if(!on){
        on = true;
      }else{
        on = false;
      }
    }
    println(name +":"+ on);
  }
}
