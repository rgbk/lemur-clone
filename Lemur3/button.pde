class Button extends Module{
  boolean on, toggleOnAdd, toggleOnRemove;

  Button(int x, int y, int w, int h, color c, boolean toggleOnAdd, boolean toggleOnRemove){
    super(x,y,w,h,c);
    this.toggleOnAdd = toggleOnAdd;
    this.toggleOnRemove = toggleOnRemove;
    midi_controller = new int[1];
    midi_controller[0] = next_available_midi;
    next_available_midi++;
  }

  void draw(){
    super.draw();
    if(on){
      noStroke();
      fill(backlightOn);
      rect(x,y,w,h);
      fill(c);
      int radius = min(w,h) / 2;
      smooth();
      ellipse(x+w/2, y+h/2, radius, radius);
    }else{
      fill(backlightOff);
      noStroke();
      rect(x,y,w,h);
    }
  }
  
  void toggle(){
    if(!on){
      on = true;
      output.sendController(0,midi_controller[0],127);
    }else{
      on = false;
      output.sendController(0,midi_controller[0],0);
    }
  }

  void addCollision(int id, int x, int y){
    if(toggleOnAdd){
      toggle();
    }
  }
  
  void removeCollision(){
    if(toggleOnRemove){
      toggle();
    }
  }
}
