int next_available_midi = 0;

class Module{
  long cursorID = -1;
  int[] midi_controller;
  int x, y, w, h;
  color c, backlightOn, backlightOff;
  boolean active;
  
  Module(int x, int y, int w, int h, color c){
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.c = c;
    backlightOn = color(red(c),green(c),blue(c),120);
    backlightOff = color(red(c),green(c),blue(c),30);
  }

  void draw(){
    stroke(c);
    strokeWeight(1);
    noFill();
    noSmooth();
    rectMode(CORNER);
    rect(x+2,y+2,w-5,h-5);
  }
  
  boolean collision(int cx, int cy){
    if(cx > x && cx < x+w && cy > y && cy < y+h){
      return true;
    }else{
      return false;
    }
  }
  void addCollision(int cursorID, int ax, int ay){ active = true; }
  void updateCollision(int ax, int ay, int bx, int by){}
  void removeCollision(int cursorID){ active = false; }
}
