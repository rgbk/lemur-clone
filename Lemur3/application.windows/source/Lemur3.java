import processing.core.*; 
import processing.xml.*; 

import TUIO.*; 
import rwmidi.*; 

import java.applet.*; 
import java.awt.*; 
import java.awt.image.*; 
import java.awt.event.*; 
import java.io.*; 
import java.net.*; 
import java.text.*; 
import java.util.*; 
import java.util.zip.*; 
import java.util.regex.*; 

public class Lemur3 extends PApplet {


TuioProcessing tuioClient;

MidiInput input;
MidiOutput output;
float cur_size = 5;

int module_count = 4;
Module[] Modules = new Module[module_count];
PFont font;

public void setup(){
  noLoop();
  tuioClient  = new TuioProcessing(this);
  String[] out = RWMidi.getOutputDeviceNames();
  println(out);  
  output = RWMidi.getOutputDevices()[0].createOutput();

  String[] in = RWMidi.getInputDeviceNames();
  println(in);
  input = RWMidi.getInputDevices()[0].createInput(this);
  font = loadFont("BitstreamVeraSans-Roman-10.vlw");
  textFont(font);
  size(640,480);

  Modules[0] = new Knob(0,0,320,240,0xffff2040);
  Modules[1] = new Button(320,0,320,240,0xff40ff40,true,true);
  Modules[2] = new Slider(0,240,320,240,0xff1060e0,"horizontal","absolute");
  Modules[3] = new Monome(320,240,320,240,0xffffffff,4,4);

/*  Modules[0] = new Knob(0,   0,   160,240,#F54D2F);
  Modules[1] = new Knob(160, 0,   160,240,#C3F52F);
  Modules[2] = new Knob(320, 0,   160,240,#2FA0F5);
  Modules[3] = new Knob(480, 0,   160,240,#4D4D4D);
  Modules[4] = new Button(0,   240, 160,240,#F54D2F,true,false);
  Modules[5] = new Button(160, 240, 160,240,#C3F52F,true,false);
  Modules[6] = new Button(320, 240, 160,240,#2FA0F5,true,false);
  Modules[7] = new Button(480, 240, 160,240,#4D4D4D,true,false);*/
}

public void draw(){
  background(0xff272624);
  strokeWeight(1);
  for(int i=0; i< module_count; i++){
    Modules[i].draw();
  }
  
  smooth();
  strokeWeight(2);
  Vector tuioCursorList = tuioClient.getTuioCursors();
  for (int i=0;i<tuioCursorList.size();i++) {
    TuioCursor tcur = (TuioCursor)tuioCursorList.elementAt(i);
    Vector pointList = tcur.getPath();
    if (pointList.size()>0) {
      stroke(23,206,250,33);
      noFill();
      beginShape();
      for (int j=0;j<pointList.size();j++) {
        TuioPoint this_point = (TuioPoint)pointList.elementAt(j);
        vertex(this_point.getScreenX(width),this_point.getScreenY(height));
      }
      endShape();
        
      noStroke();
      fill(192,192,192);
      ellipse( tcur.getScreenX(width), tcur.getScreenY(height),cur_size,cur_size);
      fill(255);
      text(""+ tcur.getCursorID(),  tcur.getScreenX(width)+2,  tcur.getScreenY(height)+4);
    }
  }
}
// ADD
public void addTuioCursor(TuioCursor tcur) {
  //print('<');
  //println("add cursor "+tcur.getCursorID()+" ("+tcur.getSessionID()+ ") " +tcur.getX()+" "+tcur.getY());
  Vector pointList = tcur.getPath();
  if (pointList.size()>0) {
    TuioPoint A = (TuioPoint)pointList.elementAt(0);
    for(int i=0; i<module_count; i++){
      if(Modules[i].collision(A.getScreenX(width),A.getScreenY(height))){
        Modules[i].cursorID = tcur.getCursorID();
        Modules[i].addCollision(tcur.getCursorID(),A.getScreenX(width),A.getScreenY(height));
      }
    }
  }
}

// MOVE
public void updateTuioCursor (TuioCursor tcur) {
  Vector tuioCursorList = tuioClient.getTuioCursors();
  //print(tuioCursorList.size());
  //println("update cursor "+tcur.getCursorID()+" ("+tcur.getSessionID()+ ") " +tcur.getX()+" "+tcur.getY() +" "+tcur.getMotionSpeed()+" "+tcur.getMotionAccel());
  Vector pointList = tcur.getPath();
  if (pointList.size()>1) {
    TuioPoint A = (TuioPoint)pointList.elementAt(pointList.size()-2);
    TuioPoint B = (TuioPoint)pointList.elementAt(pointList.size()-1);
    for(int i=0; i<module_count; i++){  
      if(tcur.getCursorID() == Modules[i].cursorID){
        Modules[i].updateCollision(A.getScreenX(width),A.getScreenY(height),B.getScreenX(width),B.getScreenY(height));
      }
    }
  }
}

// REMOVE
public void removeTuioCursor(TuioCursor tcur) {
  //print('>');
  //println("remove cursor "+tcur.getCursorID()+" ("+tcur.getSessionID()+")");
  
  Vector pointList = tcur.getPath();
  if (pointList.size()>0) {
    TuioPoint B = (TuioPoint)pointList.elementAt(pointList.size()-1);
    for(int i=0; i<module_count; i++){
      if(tcur.getCursorID() == Modules[i].cursorID){
        Modules[i].cursorID = -1;
        Modules[i].removeCollision(tcur.getCursorID());
      }
    }
  }
}

// called after each message bundle
// representing the end of an image frame
public void refresh(TuioTime bundleTime) { 
  redraw();
}

// i don't care about objects...
public void addTuioObject(TuioObject tobj) {}
public void removeTuioObject(TuioObject tobj) {}
public void updateTuioObject(TuioObject tobj) {}
class Button extends Module{
  boolean on, toggleOnAdd, toggleOnRemove;

  Button(int x, int y, int w, int h, int c, boolean toggleOnAdd, boolean toggleOnRemove){
    super(x,y,w,h,c);
    this.toggleOnAdd = toggleOnAdd;
    this.toggleOnRemove = toggleOnRemove;
    midi_controller = new int[1];
    midi_controller[0] = next_available_midi;
    next_available_midi++;
  }

  public void draw(){
    super.draw();
    if(on){
      noStroke();
      fill(backlightOn);
      rect(x,y,w,h);
      fill(c);
      int radius = min(w,h) / 2;
      smooth();
      ellipse(x+w/2, y+h/2, radius, radius);
    }else{
      fill(backlightOff);
      noStroke();
      rect(x,y,w,h);
    }
  }
  
  public void toggle(){
    if(!on){
      on = true;
      output.sendController(0,midi_controller[0],127);
    }else{
      on = false;
      output.sendController(0,midi_controller[0],0);
    }
  }

  public void addCollision(int id, int x, int y){
    if(toggleOnAdd){
      toggle();
    }
  }
  
  public void removeCollision(){
    if(toggleOnRemove){
      toggle();
    }
  }
}

class Knob extends Module{
  float angle = -PI/2;
  float radius;
  float lowerLimit = -PI - PI / 4.0f;
  float upperLimit = PI / 4.0f;
  float centerX, centerY;

  Knob(int x, int y, int w, int h, int c){
    super(x,y,w,h,c);
    centerX = x + w/2;
    centerY = y + h/2;
    this.radius = min(w,h);
    midi_controller = new int[1];
    midi_controller[0] = next_available_midi;
    next_available_midi++;
  }

  public void draw(){
 //   super.draw();
    smooth();
    pushMatrix();
      translate(centerX, centerY);
      //draw the fill
      noStroke();
      if(active)
        fill(backlightOn);
      else
        fill(backlightOff);
      ellipse(0,0,radius*0.9f,radius*0.9f);        
      //draw the outline
      stroke(c);
      noFill();
      strokeWeight(1);
      ellipse(0,0,radius*0.9f,radius*0.9f);
      //draw the progress arc
      strokeWeight(radius*0.1f);
      strokeCap(SQUARE);
      arc(0,0,PApplet.parseInt(radius*0.6f),PApplet.parseInt(radius*0.6f),lowerLimit,angle);
      fill(c);
      ellipse(0,0,radius*0.1f, radius*0.1f);
    popMatrix();
  }
  
  public void updateCollision(int ax, int ay, int bx, int by){
    float centerX = x+w/2;
    float centerY = y+h/2;
    float prevAngle = atan2(ay-centerY, ax-centerX);
    float thisAngle = atan2(by-centerY, bx-centerX);
    float diffAngle = thisAngle - prevAngle;
    if(diffAngle >=  PI) diffAngle -= TWO_PI;
    if(diffAngle <= -PI) diffAngle += TWO_PI;
    angle += diffAngle;

    angle = min(upperLimit ,angle);
    angle = max(lowerLimit ,angle);
    output.sendController(0,midi_controller[0],round(map(angle,lowerLimit,upperLimit,0,127)));
  }
}
int next_available_midi = 0;

class Module{
  long cursorID = -1;
  int[] midi_controller;
  int x, y, w, h;
  int c, backlightOn, backlightOff;
  boolean active;
  
  Module(int x, int y, int w, int h, int c){
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.c = c;
    backlightOn = color(red(c),green(c),blue(c),120);
    backlightOff = color(red(c),green(c),blue(c),30);
  }

  public void draw(){
    stroke(c);
    strokeWeight(1);
    noFill();
    noSmooth();
    rectMode(CORNER);
    rect(x+2,y+2,w-5,h-5);
  }
  
  public boolean collision(int cx, int cy){
    if(cx > x && cx < x+w && cy > y && cy < y+h){
      return true;
    }else{
      return false;
    }
  }
  public void addCollision(int cursorID, int ax, int ay){ active = true; }
  public void updateCollision(int ax, int ay, int bx, int by){}
  public void removeCollision(int cursorID){ active = false; }
}
class Monome extends Module{
  boolean[] on;
  int[] cursors, notes;
  int resX,resY;
  
  Monome(int x, int y, int w, int h, int c, int resX, int resY){
    super(x,y,w,h,c);
    this.resX = resX;
    this.resY = resY;
    cursors = new int[32];
    notes = new int[resX * resY];
    
    for(int i=0; i<(resX * resY); i++){
      notes[i] = i;
    }
    
 /*   String[] noteArray = {
      "c#2","d#2","f#2","g#2","a#2",
      "c#3","d#3","f#3","g#3","a#3",
      "c#4","d#4","f#4","g#4","a#4",
      "c#5","d#5","f#5","g#5","a#5",
      "c#6","d#6","f#6","g#6","a#6"};
      
    for(int i=0; i<noteArray.length; i++){
      int midi = 0;
      if(noteArray[i].indexOf('d') != -1)
        midi += 2;
      if(noteArray[i].indexOf('e') != -1)
        midi += 4;
      if(noteArray[i].indexOf('f') != -1)
        midi += 5;
      if(noteArray[i].indexOf('g') != -1)
        midi += 7;
      if(noteArray[i].indexOf('a') != -1)
        midi += 9;
      if(noteArray[i].indexOf('b') != -1)
        midi += 11;
      if(noteArray[i].indexOf('#') != -1)
        midi += 1;
      int char_to_int = int(map(int(noteArray[i].charAt(noteArray[i].length()-1)),48,58,0,10));
      midi += char_to_int * 12;
      notes[i] = midi;
    }*/
  }
  
  public void draw(){
    super.draw();
    for(int ix=0; ix<resX; ix++){
      for(int iy=0; iy<resY; iy++){
        fill(round((float)(ix+iy*resX)/(resX*resY)*127));
        rect(x + (float)ix/resX * w +2, y + (float)iy/resY * h +2,(w-1)/resX-5,(h-1)/resY-5);
      }
    }
  }
  
  public void addCollision(int cursorID, int ax, int ay){
    int xMap = floor(map(ax,x,x+w,0,resX));
    int yMap = floor(map(ay,y,y+h,0,resY));
    int padID = xMap+yMap*resX;
    println("x: " + xMap + "  y: " + yMap + "  id: " + cursorID);
    pressPad(padID);
    cursors[cursorID] = padID;
  }
  
  public void removeCollision(int cursorID){
    releasePad(cursors[cursorID]);
  }
  
  public void pressPad(int id){
    int send = output.sendNoteOn(0, notes[id], 100);
  }
  
  public void releasePad(int id){
    int send = output.sendNoteOff(0, notes[id], 100);
  }
}
class Slider extends Module{
  boolean horizontal, vertical, absolute;
  int handle_w, handle_h;
  float valueX, valueY;

  Slider(int x, int y, int w, int h, int c, String mode, String absolute){
    super(x,y,w,h,c);
    
    if(mode.equals("horizontal")){
      horizontal = true;
      vertical = false;
      handle_h = h-3;
      handle_w = min(w/6, 15);
      valueX = 0.5f;
      midi_controller = new int[1];
      midi_controller[0] = next_available_midi;
      next_available_midi++;
    }
    
    if(mode.equals("vertical")){
      horizontal = false;
      vertical = true;
      handle_h = min(h/6, 15);
      handle_w = w-3;
      valueY = 0.5f;
      midi_controller = new int[1];
      midi_controller[0] = next_available_midi;
      next_available_midi++;
    }
    
    if(mode.equals("xy")){
      horizontal = true;
      vertical = true;
      int smaller = min(w,h);
      handle_h = min(smaller/6, 16);
      handle_w = min(smaller/6, 16);
      valueX = 0.5f;
      valueY = 0.5f;
      midi_controller = new int[2];
      midi_controller[0] = next_available_midi;
      midi_controller[1] = next_available_midi;
      next_available_midi++;
    }
    
    if(absolute.equals("absolute")){
      this.absolute = true;
    }
  }
  
  public void draw(){
    super.draw();
    fill(c);
    noStroke();
    
    float xVal = x + w*valueX;
    float yVal = y + h*valueY;
    

    if(horizontal && !vertical){
      rect(xVal, y+2, handle_w,handle_h);
    }
    if(vertical && !horizontal){
      rect(x+2, yVal, handle_w,handle_h);
    }
    if(horizontal && vertical){
      rectMode(CENTER);
      rect(xVal, yVal, handle_w,handle_h);
      stroke(c);
      line(x,yVal,x+w,yVal);
      line(xVal,y,xVal,y+h);
    }
  }
  
  public void updateCollision(int ax, int ay, int bx, int by){
    if(absolute){
      if(horizontal){
        valueX = (float)(bx-x)/w;
      }
      if(vertical){
        valueY = (float)(by-y)/h;
      }
    }else{
      if(horizontal){
        if(valueX >= 0 && valueX <= 1)
          valueX += PApplet.parseFloat(bx-ax)/w;
      }
      if(vertical){
        if(valueY >= 0 && valueY <= 1)
          valueY += PApplet.parseFloat(by-ay)/h;
      }
    }
    
    valueX = max(valueX, 0);
    valueX = min(valueX, 1);
    valueY = max(valueY, 0);
    valueY = min(valueY, 1);
    if((horizontal && !vertical) || (!horizontal && vertical)){
      float value = max(valueX,valueY);
      output.sendController(0,midi_controller[0],round(value*127));
    }
    if(horizontal && vertical){
       output.sendController(0,midi_controller[0],round(valueX*127));
       output.sendController(0,midi_controller[1],round(valueY*127));
    }
  }
}

  static public void main(String args[]) {
    PApplet.main(new String[] { "--present", "--bgcolor=#666666", "--hide-stop", "Lemur3" });
  }
}
