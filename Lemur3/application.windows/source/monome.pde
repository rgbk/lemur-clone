class Monome extends Module{
  boolean[] on;
  int[] cursors, notes;
  int resX,resY;
  
  Monome(int x, int y, int w, int h, color c, int resX, int resY){
    super(x,y,w,h,c);
    this.resX = resX;
    this.resY = resY;
    cursors = new int[32];
    notes = new int[resX * resY];
    
    for(int i=0; i<(resX * resY); i++){
      notes[i] = i;
    }
    
 /*   String[] noteArray = {
      "c#2","d#2","f#2","g#2","a#2",
      "c#3","d#3","f#3","g#3","a#3",
      "c#4","d#4","f#4","g#4","a#4",
      "c#5","d#5","f#5","g#5","a#5",
      "c#6","d#6","f#6","g#6","a#6"};
      
    for(int i=0; i<noteArray.length; i++){
      int midi = 0;
      if(noteArray[i].indexOf('d') != -1)
        midi += 2;
      if(noteArray[i].indexOf('e') != -1)
        midi += 4;
      if(noteArray[i].indexOf('f') != -1)
        midi += 5;
      if(noteArray[i].indexOf('g') != -1)
        midi += 7;
      if(noteArray[i].indexOf('a') != -1)
        midi += 9;
      if(noteArray[i].indexOf('b') != -1)
        midi += 11;
      if(noteArray[i].indexOf('#') != -1)
        midi += 1;
      int char_to_int = int(map(int(noteArray[i].charAt(noteArray[i].length()-1)),48,58,0,10));
      midi += char_to_int * 12;
      notes[i] = midi;
    }*/
  }
  
  void draw(){
    super.draw();
    for(int ix=0; ix<resX; ix++){
      for(int iy=0; iy<resY; iy++){
        fill(round((float)(ix+iy*resX)/(resX*resY)*127));
        rect(x + (float)ix/resX * w +2, y + (float)iy/resY * h +2,(w-1)/resX-5,(h-1)/resY-5);
      }
    }
  }
  
  void addCollision(int cursorID, int ax, int ay){
    int xMap = floor(map(ax,x,x+w,0,resX));
    int yMap = floor(map(ay,y,y+h,0,resY));
    int padID = xMap+yMap*resX;
    println("x: " + xMap + "  y: " + yMap + "  id: " + cursorID);
    pressPad(padID);
    cursors[cursorID] = padID;
  }
  
  void removeCollision(int cursorID){
    releasePad(cursors[cursorID]);
  }
  
  void pressPad(int id){
    int send = output.sendNoteOn(0, notes[id], 100);
  }
  
  void releasePad(int id){
    int send = output.sendNoteOff(0, notes[id], 100);
  }
}
