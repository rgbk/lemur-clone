import TUIO.*;
TuioProcessing tuioClient;
import rwmidi.*;
MidiInput input;
MidiOutput output;
float cur_size = 5;

int module_count = 4;
Module[] Modules = new Module[module_count];
PFont font;

void setup(){
  noLoop();
  tuioClient  = new TuioProcessing(this);
  String[] out = RWMidi.getOutputDeviceNames();
  println(out);  
  output = RWMidi.getOutputDevices()[0].createOutput();

  String[] in = RWMidi.getInputDeviceNames();
  println(in);
  input = RWMidi.getInputDevices()[0].createInput(this);
  font = loadFont("BitstreamVeraSans-Roman-10.vlw");
  textFont(font);
  size(640,480);

  Modules[0] = new Knob(0,0,320,240,#ff2040);
  Modules[1] = new Button(320,0,320,240,#40ff40,true,true);
  Modules[2] = new Slider(0,240,320,240,#1060e0,"horizontal","absolute");
  Modules[3] = new Monome(320,240,320,240,#ffffff,4,4);

/*  Modules[0] = new Knob(0,   0,   160,240,#F54D2F);
  Modules[1] = new Knob(160, 0,   160,240,#C3F52F);
  Modules[2] = new Knob(320, 0,   160,240,#2FA0F5);
  Modules[3] = new Knob(480, 0,   160,240,#4D4D4D);
  Modules[4] = new Button(0,   240, 160,240,#F54D2F,true,false);
  Modules[5] = new Button(160, 240, 160,240,#C3F52F,true,false);
  Modules[6] = new Button(320, 240, 160,240,#2FA0F5,true,false);
  Modules[7] = new Button(480, 240, 160,240,#4D4D4D,true,false);*/
}

void draw(){
  background(#272624);
  strokeWeight(1);
  for(int i=0; i< module_count; i++){
    Modules[i].draw();
  }
  
  smooth();
  strokeWeight(2);
  Vector tuioCursorList = tuioClient.getTuioCursors();
  for (int i=0;i<tuioCursorList.size();i++) {
    TuioCursor tcur = (TuioCursor)tuioCursorList.elementAt(i);
    Vector pointList = tcur.getPath();
    if (pointList.size()>0) {
      stroke(23,206,250,33);
      noFill();
      beginShape();
      for (int j=0;j<pointList.size();j++) {
        TuioPoint this_point = (TuioPoint)pointList.elementAt(j);
        vertex(this_point.getScreenX(width),this_point.getScreenY(height));
      }
      endShape();
        
      noStroke();
      fill(192,192,192);
      ellipse( tcur.getScreenX(width), tcur.getScreenY(height),cur_size,cur_size);
      fill(255);
      text(""+ tcur.getCursorID(),  tcur.getScreenX(width)+2,  tcur.getScreenY(height)+4);
    }
  }
}
