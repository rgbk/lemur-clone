// ADD
void addTuioCursor(TuioCursor tcur) {
  //print('<');
  //println("add cursor "+tcur.getCursorID()+" ("+tcur.getSessionID()+ ") " +tcur.getX()+" "+tcur.getY());
  Vector pointList = tcur.getPath();
  if (pointList.size()>0) {
    TuioPoint A = (TuioPoint)pointList.elementAt(0);
    for(int i=0; i<module_count; i++){
      if(Modules[i].collision(A.getScreenX(width),A.getScreenY(height))){
        Modules[i].cursorID = tcur.getCursorID();
        Modules[i].addCollision(tcur.getCursorID(),A.getScreenX(width),A.getScreenY(height));
      }
    }
  }
}

// MOVE
void updateTuioCursor (TuioCursor tcur) {
  Vector tuioCursorList = tuioClient.getTuioCursors();
  //print(tuioCursorList.size());
  //println("update cursor "+tcur.getCursorID()+" ("+tcur.getSessionID()+ ") " +tcur.getX()+" "+tcur.getY() +" "+tcur.getMotionSpeed()+" "+tcur.getMotionAccel());
  Vector pointList = tcur.getPath();
  if (pointList.size()>1) {
    TuioPoint A = (TuioPoint)pointList.elementAt(pointList.size()-2);
    TuioPoint B = (TuioPoint)pointList.elementAt(pointList.size()-1);
    for(int i=0; i<module_count; i++){  
      if(tcur.getCursorID() == Modules[i].cursorID){
        Modules[i].updateCollision(A.getScreenX(width),A.getScreenY(height),B.getScreenX(width),B.getScreenY(height));
      }
    }
  }
}

// REMOVE
void removeTuioCursor(TuioCursor tcur) {
  //print('>');
  //println("remove cursor "+tcur.getCursorID()+" ("+tcur.getSessionID()+")");
  
  Vector pointList = tcur.getPath();
  if (pointList.size()>0) {
    TuioPoint B = (TuioPoint)pointList.elementAt(pointList.size()-1);
    for(int i=0; i<module_count; i++){
      if(tcur.getCursorID() == Modules[i].cursorID){
        Modules[i].cursorID = -1;
        Modules[i].removeCollision(tcur.getCursorID());
      }
    }
  }
}

// called after each message bundle
// representing the end of an image frame
void refresh(TuioTime bundleTime) { 
  redraw();
}

// i don't care about objects...
void addTuioObject(TuioObject tobj) {}
void removeTuioObject(TuioObject tobj) {}
void updateTuioObject(TuioObject tobj) {}
