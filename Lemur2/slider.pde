class Slider extends Module{
  boolean horizontal, vertical;
  int handle_w, handle_h;
  float valueX, valueY;

  Slider(int x, int y, int w, int h, color c, String mode, String relative){
    super(x,y,w,h,c);
    
    if(mode.equals("horizontal")){
      horizontal = true;
      vertical = false;
      handle_h = h-3;
      handle_w = min(w/6, 15);
      valueX = 0.5;
    }
    
    if(mode.equals("vertical")){
      horizontal = false;
      vertical = true;
      handle_h = min(h/6, 15);
      handle_w = w-3;
      valueY = 0.5;
    }
    
    if(mode.equals("xy")){
      horizontal = true;
      vertical = true;
      int smaller = min(w,h);
      handle_h = min(smaller/6, 16);
      handle_w = min(smaller/6, 16);
      valueX = 0.5;
      valueY = 0.5;
    }
  }
  
  void draw(){
    super.draw();
    fill(c);
    noStroke();
    
    float xVal = x + w*valueX;
    float yVal = y + h*valueY;
    

    if(horizontal && !vertical){
      rect(xVal, y+2, handle_w,handle_h);
    }
    if(vertical && !horizontal){
      rect(x+2, yVal, handle_w,handle_h);
    }
    if(horizontal && vertical){
      rectMode(CENTER);
      rect(xVal, yVal, handle_w,handle_h);
      stroke(c);
      line(x,yVal,x+w,yVal);
      line(xVal,y,xVal,y+h);
    }
  }
  
  void updateCollision(int ax, int ay, int bx, int by){
    if(horizontal){
      if(valueX >= 0 && valueX <= 1)
        valueX += float(bx-ax)/w;
        valueX = max(valueX, 0);
        valueX = min(valueX, 1);
    }
    if(vertical){
      if(valueY >= 0 && valueY <= 1)
        valueY += float(by-ay)/h;
        valueY = max(valueY, 0);
        valueY = min(valueY, 1);
    }
  }
}
