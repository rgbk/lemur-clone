class Knob extends Module{
  float angle = -PI/2;
  float radius;
  float line_length;

  Knob(int x, int y, int w, int h, color c){
    super(x,y,w,h,c);
    this.radius = min(w,h);
    this.line_length = (radius * 0.6) * 0.5 ;//+ (radius*0.05);
  }

  void draw(){
    //super.draw();
    smooth();
    float centerX = x+w/2;
    float centerY = y+h/2;
    pushMatrix();
      translate(centerX, centerY);
      //draw the fill
      noStroke();
      if(active)
        fill(backlightOn);
      else
        fill(backlightOff);
      ellipse(0,0,radius*0.9,radius*0.9);        
      //draw the outline
      stroke(c);
      noFill();
      strokeWeight(1);
      ellipse(0,0,radius*0.9,radius*0.9);
      //draw the progress arc
      strokeWeight(radius*0.1);
      strokeCap(SQUARE);
      arc(0,0,radius*0.6,radius*0.6,-PI*1.5,angle);
      //draw the line
      rotate(angle);
      strokeCap(ROUND);
      line(0,0,line_length,0);
    popMatrix();
  }
  
  void updateCollision(int ax, int ay, int bx, int by){
    float centerX = x+w/2;
    float centerY = y+h/2;
    float prevAngle = atan2(ay-centerY, ax-centerX);
    float thisAngle = atan2(by-centerY, bx-centerX);
    angle += thisAngle - prevAngle;
  }
}
