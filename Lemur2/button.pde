class Button extends Module{
  boolean on, toggleOnAdd, toggleOnRemove;

  Button(int x, int y, int w, int h, color c, boolean toggleOnAdd, boolean toggleOnRemove){
    super(x,y,w,h,c);
    this.toggleOnAdd = toggleOnAdd;
    this.toggleOnRemove = toggleOnRemove;
  }

  void draw(){
    super.draw();
    if(on){
      noStroke();
      fill(backlightOn);
      rect(x,y,w,h);
      fill(c);
      int radius = min(w,h) / 2;
      smooth();
      ellipse(x+w/2, y+h/2, radius, radius);
    }else{
      fill(backlightOff);
      noStroke();
      rect(x,y,w,h);
    }
  }
  
  void toggle(){
    if(!on){
      on = true;
    }else{
      on = false;
    }
  }

  void addCollision(){
    if(toggleOnAdd){
      toggle();
    }
  }
  
  void removeCollision(){
    if(toggleOnRemove){
      toggle();
    }
  }
}
