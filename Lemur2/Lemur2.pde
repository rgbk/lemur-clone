import TUIO.*;
TuioProcessing tuioClient;
float cur_size = 5;

int module_count = 14;
Module[] Modules = new Module[module_count];
PFont font;

void setup(){
  noLoop();
  tuioClient  = new TuioProcessing(this);
  font = loadFont("BitstreamVeraSans-Roman-10.vlw");
  textFont(font);
  size(640,480);

  //Pads
  Modules[0] = new Slider(55,32,175,121,#758674,"xy","absolute");
  Modules[1] = new Button(410,32,175,121,#758674,true,false);

  //Turntables
  Modules[2] = new Knob(57,172,220,220,#e8b537);
  Modules[3] = new Knob(362,172,220,220,#e8b537);

  //Filter knobs
  Modules[4] = new Knob(245,39,56,56,#37e8cd);
  Modules[5] = new Knob(308,71,53,53,#45e8b0);
  Modules[6] = new Knob(263,117,50,50,#53e892);
  Modules[7] = new Knob(330,140,47,47,#5be880);
  Modules[8] = new Knob(284,182,44,44,#68e865);
  Modules[9] = new Knob(330,208,41,41,#70e855);
  Modules[10] = new Knob(297,248,38,38,#7ae83f);
  
  //Fader
  Modules[11] = new Slider(230,405,175,40,#e837a3,"horizontal","relative");
  
  //Kill Buttons
  Modules[12] = new Button(180,405,40,40,#ffffff,true,true);
  Modules[13] = new Button(415,405,40,40,#ffffff,true,true);
}

void draw(){
  background(#272624);
  strokeWeight(1);
  for(int i=0; i< module_count; i++){
    Modules[i].draw();
  }
  
  smooth();
  strokeWeight(2);
  Vector tuioCursorList = tuioClient.getTuioCursors();
  for (int i=0;i<tuioCursorList.size();i++) {
    TuioCursor tcur = (TuioCursor)tuioCursorList.elementAt(i);
    Vector pointList = tcur.getPath();
    if (pointList.size()>0) {
      stroke(23,206,250,33);
      noFill();
      beginShape();
      for (int j=0;j<pointList.size();j++) {
        TuioPoint this_point = (TuioPoint)pointList.elementAt(j);
        vertex(this_point.getScreenX(width),this_point.getScreenY(height));
      }
      endShape();
        
      noStroke();
      fill(192,192,192);
      ellipse( tcur.getScreenX(width), tcur.getScreenY(height),cur_size,cur_size);
      fill(255);
      text(""+ tcur.getCursorID(),  tcur.getScreenX(width)+2,  tcur.getScreenY(height)+4);
    }
  }
}
