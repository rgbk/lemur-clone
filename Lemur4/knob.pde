class Knob extends Module{
  float angle = -PI/2;
  float radius;
  float lowerLimit = -PI - PI / 4.0f;
  float upperLimit = PI / 4.0f;
  float centerX, centerY;

  Knob(int x, int y, int w, int h, color c){
    super(x,y,w,h,c);
    centerX = x + w/2;
    centerY = y + h/2;
    this.radius = min(w,h);
    midi_controller = new int[1];
    midi_controller[0] = next_available_midi;
    next_available_midi++;
  }

  void draw(){
 //   super.draw();
    smooth();
    pushMatrix();
      translate(centerX, centerY);
      //draw the fill
      noStroke();
      if(active)
        fill(backlightOn);
      else
        fill(backlightOff);
      ellipse(0,0,radius*0.9,radius*0.9);        
      //draw the outline
      stroke(c);
      noFill();
      strokeWeight(1);
      ellipse(0,0,radius*0.9,radius*0.9);
      //draw the progress arc
      strokeWeight(radius*0.1);
      strokeCap(SQUARE);
      arc(0,0,int(radius*0.6),int(radius*0.6),lowerLimit,angle);
      fill(c);
      ellipse(0,0,radius*0.1, radius*0.1);
    popMatrix();
  }
  
  void updateCollision(int ax, int ay, int bx, int by){
    float centerX = x+w/2;
    float centerY = y+h/2;
    float prevAngle = atan2(ay-centerY, ax-centerX);
    float thisAngle = atan2(by-centerY, bx-centerX);
    float diffAngle = thisAngle - prevAngle;
    if(diffAngle >=  PI) diffAngle -= TWO_PI;
    if(diffAngle <= -PI) diffAngle += TWO_PI;
    angle += diffAngle;

    angle = min(upperLimit ,angle);
    angle = max(lowerLimit ,angle);
    output.sendController(0,midi_controller[0],round(map(angle,lowerLimit,upperLimit,0,127)));
  }
}
